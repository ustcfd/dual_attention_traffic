# coding=utf-8
import torch
from collections import OrderedDict
import utils
import torch
from torch.autograd import Variable
import os

# single gpu restore model
def predict_singlegpu(model,opt,test_dataloader,scaler,predtime):
   if opt.save_mode == 'best':
        model_name = opt.save_mode + '_' + predtime +'_'+opt.task+ '_' + opt.loss_func + '.chkpt'
        modelfile_path = os.path.join(opt.checkpointDir, model_name)

   checkpoint = torch.load(modelfile_path)
   new_state_dict = OrderedDict()
   for k, v in checkpoint['model'].items():
       name = k[7:]  # remove module.
       new_state_dict[name] = v
   model.load_state_dict(new_state_dict)
   use_gpu = torch.cuda.is_available()
   total_loss = 0
   step = 0
   for data in test_dataloader:
       inputs, labels = data
       step += 1
       if inputs.shape[0] != opt.batch_size:
           continue
       if use_gpu:
           inputs, labels = Variable(inputs.cuda(), volatile=True), Variable(labels.cuda(), volatile=True)
       else:
           inputs, labels = Variable(inputs, volatile=True), Variable(labels, volatile=True)
       pred = model(inputs, labels, is_training=False)
       labels = torch.transpose(torch.squeeze(labels), 1, 2)
       if opt.loss_func == 'MAE':
           pred_loss = utils.masked_mae_loss(scaler)(grads=labels, preds=pred)
       elif opt.loss_func == 'RMSE':
           pred_loss = utils.masked_rmse_loss(scaler)(grads=labels, preds=pred)
       else:  # mape
           pred_loss = utils.masked_mape_loss(scaler)(grads=labels, preds=pred)
       total_loss += pred_loss.data[0]
   final_loss = total_loss / step
   print('(Predicting) single gpu final average {} loss: {loss:3.3f}'.format(opt.loss_func,loss=final_loss))
   log_pred_file = os.path.join(opt.summaryDir, opt.task + '.pred.log')
   with open(log_pred_file, 'a') as log_tf:
       log_tf.write('In {} step ahead forecasting,the single gpu predict {} final 20% testdataset {} loss:{loss: 8.4f}\n'
                    .format(opt.seq_len,predtime,opt.loss_func,loss=final_loss))
   return final_loss

def predict_multigpu(model,opt,test_dataloader,scaler,predtime):
   if opt.save_mode == 'best':
        model_name = opt.save_mode + '_' + predtime +'_'+opt.task+'_' + opt.loss_func + '.chkpt'
        modelfile_path = os.path.join(opt.checkpointDir, model_name)

   checkpoint = torch.load(modelfile_path)
   model.load_state_dict(checkpoint['model'])
   use_gpu = torch.cuda.is_available()
   total_loss = 0
   step = 0
   for data in test_dataloader:
       inputs, labels = data
       step += 1
       if inputs.shape[0] != opt.batch_size:
           continue
       if use_gpu:
           inputs, labels = Variable(inputs.cuda(), volatile=True), Variable(labels.cuda(), volatile=True)
       else:
           inputs, labels = Variable(inputs, volatile=True), Variable(labels, volatile=True)
       pred = model(inputs, labels, is_training=False)
       labels = torch.transpose(torch.squeeze(labels), 1, 2)
       if opt.loss_func == 'MAE':
           pred_loss = utils.masked_mae_loss(scaler)(grads=labels, preds=pred)
       elif opt.loss_func == 'RMSE':
           pred_loss = utils.masked_rmse_loss(scaler)(grads=labels, preds=pred)
       else:  # mape
           pred_loss = utils.masked_mape_loss(scaler)(grads=labels, preds=pred)
       total_loss += pred_loss.data[0]
   final_loss = total_loss / step
   print('(Predicting) multi gpu final average {} loss: {loss:3.3f}'.format(opt.loss_func,loss=final_loss))
   log_pred_file = os.path.join(opt.summaryDir, opt.task + '.pred.log')
   with open(log_pred_file, 'a') as log_tf:
       log_tf.write('In {} step ahead forecasting,the multi gpu predict {} final 20% testdataset {} loss:{loss: 8.4f}\n'
           .format(opt.seq_len, predtime, opt.loss_func, loss=final_loss))
   return final_loss

