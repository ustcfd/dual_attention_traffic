# coding=utf-8
import utils
import torch
from torch.autograd import Variable
from gcrn_model import GraphAttentionModel
import time
import os
import pandas as pd
import predicter
import config
import datetime

def adjust_learning_rate(optimizer,epoch,init_lr,lr_decay_epoch=10):
    # 每10个批次衰减百分之十
    # **幂 - 返回x的y次幂.//取整除 - 返回商的整数部分
    lr = init_lr * (0.8 ** (epoch // lr_decay_epoch))
    if epoch % lr_decay_epoch == 0:
        print('LR is set to {}'.format(lr))
    for param_group in optimizer.param_groups:
        param_group['lr'] = lr

def train_epoch(model,training_data,epoch,global_step,optimizer,opt,scaler):
    ''' Epoch operation in training phase'''
    model.train()
    use_gpu = torch.cuda.is_available()
    total_loss = 0
    step=0
    for data in training_data:
        start = time.time()
        inputs, labels = data
        global_step += 1
        if inputs.shape[0] != opt.batch_size:
            continue

        if use_gpu:
            inputs, labels = Variable(inputs.cuda()), Variable(labels.cuda())
        else:
            inputs, labels = Variable(inputs), Variable(labels)
            # inputs.size()为[batch_size,Node,time_step,feat_in]=[64,132,30,1]
        # forward
        pred = model(inputs,labels,is_training=True,global_step=global_step)
        labels = torch.transpose(torch.squeeze(labels), 1, 2)
        # print(pred)
        # backward
        if opt.loss_func == 'MAE':
            train_loss = utils.masked_mae_loss(scaler)(grads=labels, preds=pred)
        elif opt.loss_func=='RMSE':
            train_loss = utils.masked_rmse_loss(scaler)(grads=labels, preds=pred)
        else:#mape
            train_loss = utils.masked_mape_loss(scaler)(grads=labels, preds=pred)
        # backward
        optimizer.zero_grad()
        train_loss.backward()
        # torch.nn.utils.clip_grad_norm(model.parameters(), 5.0)
        optimizer.step()
        # optimizer.step()
        step+=1
        total_loss += train_loss.data[0]
        print('(Training) Epoch {}/{} global step:{}/{},{} loss:{loss: 8.3f},elapse:{elapse:3.3f} min'.format(
            epoch, opt.epochs - 1,step, global_step,opt.loss_func,
            loss=train_loss.data[0],
            elapse=(time.time() - start) / 60))
    return total_loss/step,global_step

def eval_epoch(model,validation_data,opt,scaler):
    ''' Epoch operation in evaluation phase '''
    model.eval()
    use_gpu = torch.cuda.is_available()
    total_loss = 0
    step=0
    # loss_MSE = torch.nn.MSELoss()
    for data in validation_data:
        start = time.time()
        inputs, labels = data
        step+=1
        if inputs.shape[0] != opt.batch_size:
            continue
        if use_gpu:
            inputs, labels = Variable(inputs.cuda(),volatile=True), Variable(labels.cuda(),volatile=True)
        else:
            inputs, labels = Variable(inputs,volatile=True), Variable(labels,volatile=True)
        # forward
        pred = model(inputs,labels,is_training=False)
        labels=torch.transpose(torch.squeeze(labels),1,2)
        # valid_loss = utils.get_performance(torch.squeeze(labels),pred)
        if opt.loss_func == 'MAE':
            valid_loss = utils.masked_mae_loss(scaler)(grads=labels, preds=pred)
        elif opt.loss_func == 'RMSE':
            valid_loss = utils.masked_rmse_loss(scaler)(grads=labels, preds=pred)
        else:  # mape
            valid_loss = utils.masked_mape_loss(scaler)(grads=labels, preds=pred)
        # valid_loss = loss_MSE(pred, labels)
        total_loss+=valid_loss.data[0]

    return total_loss/step



def train(model, training_data, validation_data,optimizer, opt,scalar,predtime):
    log_file = None
    best_loss = 1e9
    #创建check_point和summary目录
    if not os.path.exists(opt.checkpointDir):
        os.makedirs(opt.checkpointDir)
    if not os.path.exists(opt.summaryDir):
        os.makedirs(opt.summaryDir)
    # if not os.path.exists(opt.figureDir):
    #     os.makedirs(opt.figureDir)

    # 创建训练和验证结果记录文件
    if opt.loss_func:
        log_file = os.path.join(opt.summaryDir, predtime + '.' + opt.task + '.' + opt.loss_func + '.'+'train.log')
        print('[Info] Training performance will be written to file: {} '.format(log_file))
        with open(log_file, 'w') as log_tf:
            log_tf.write('epoch,{}_train_loss,{}_valid_loss\n'.format(opt.loss_func, opt.loss_func))
    glob_step=0
    for epoch in range(opt.epochs):
        print('Epoch {}/{}'.format(epoch, opt.epochs - 1))
        print('-' * 10)
        adjust_learning_rate(optimizer,epoch,opt.learning_rate)
        train_loss,glob_step= train_epoch(model,training_data,epoch,glob_step,optimizer,opt,scalar)
        print('(Training) Epoch{}, average {} loss: {loss:3.4f}'.format(epoch,opt.loss_func,loss=train_loss))
        valid_loss = eval_epoch(model,validation_data,opt,scalar)
        print('(Validing) Epoch{}, average {} loss: {loss:3.4f}'.format(epoch,opt.loss_func,loss=valid_loss))


        if log_file:
            with open(log_file, 'a') as log_tf:
                log_tf.write('{epoch},{trainloss: 8.4f},{validloss: 8.4f}\n'.format(epoch=epoch,
                                                            trainloss=train_loss, validloss=valid_loss))

        if opt.loss_func == 'MAE' or opt.loss_func == 'RMSE':
            valid_loss = float('%.2f' % valid_loss)
        else:  # mape
            valid_loss = float('%.3f' % valid_loss)

        model_state_dict = model.state_dict()
        checkpoint = {
            'model': model_state_dict,
            'settings': opt,
            'epoch': epoch}

        if opt.save_mode == 'all':
            model_name =opt.save_mode + '_mape_{mape:3.3f}.chkpt'.format(mape=train_loss)
            modelfile_path=os.path.join(opt.checkpointDir,model_name)
            torch.save(checkpoint, modelfile_path)
        elif opt.save_mode == 'best':
            model_name = opt.save_mode+'_'+predtime+'_'+opt.task+'_'+opt.loss_func + '.chkpt'
            modelfile_path = os.path.join(opt.checkpointDir, model_name)
            if valid_loss <best_loss:
                best_loss=valid_loss
                early_stopping_epoch=0
                torch.save(checkpoint, modelfile_path)
                print('- [Info] The checkpoint file has been updated.')
            else:
                early_stopping_epoch+=1

        if early_stopping_epoch>=opt.earlystop_epochs:
            print('the best loss has not been better when at {} epochs'.format(epoch))
            break



def main():
    ''' Main function '''
    opt = config.get_config()
    # 设置预测时间范围长度
    predtime = opt.pred_len * 5
    if predtime < 60:
        predtime = str(predtime) + 'min'
    else:
        n = predtime // 60
        predtime = str(n) + 'hours'

    # ========= Preparing DataLoader =========#
    nowTime = datetime.datetime.now()  # 现在
    print('Start time:'+nowTime.strftime('%Y-%m-%d %H:%M:%S'))
    traffic_df_filename = os.path.join('data', 'df_highway_2012_4mon_sample.h5')
    adj_filename = os.path.join('data', 'sensor_graph', 'sensor_adj_mx.pkl')
    traffic_df = pd.read_hdf(traffic_df_filename)
    scaler = utils.StandardScaler(mean=traffic_df.values.mean(), std=traffic_df.values.std())
    df = scaler.transform(traffic_df)
    train_dataloader, valid_dataloader, test_dataloader = utils.PrepareDataset(df,BATCH_SIZE=opt.batch_size,seq_len=opt.seq_len,pred_len=opt.pred_len)
    num_node = len(traffic_df.columns)
    adj_bias=utils.load_adj(adj_filename,num_node)
    GPUID = '2,3'
    os.environ["CUDA_VISIBLE_DEVICES"] = GPUID
    model=GraphAttentionModel(adj_bias,feat_in=opt.feat_in, K=opt.K_head,
                              num_h=opt.num_hidden,d_sh=opt.d_sh,feat_out=opt.feat_out,res_steps=opt.res_steps,
                              coef_drop = opt.coef_drop, in_drop = opt.in_drop,ss_k=opt.k)

    # for name, param in model.state_dict().items():
    # for name, param in model.named_parameters():
    #     if param.requires_grad:
    #         print(name, param.size())

    all_parameters_num=sum([p.numel() for p in model.parameters()])
    print('Total number of parameters: '+str(all_parameters_num))
    use_gpu = torch.cuda.is_available()
    if use_gpu:
        torch.cuda.manual_seed_all(opt.manualSeed)
        train_model = torch.nn.DataParallel(model, dim=0)
        train_model=train_model.cuda()
    else:
        train_model = model

    # optimizer=torch.optim.RMSprop(build_model.parameters(),lr=opt.learning_rate)
    optimizer = torch.optim.Adam(train_model.parameters(), lr=opt.learning_rate)
    train(train_model, train_dataloader, valid_dataloader, optimizer, opt,scaler,predtime)

    # prediction single gpu
    if use_gpu:
        pred_model=model.cuda()
    predicter.predict_singlegpu(pred_model,opt,test_dataloader,scaler,predtime)

    # compute total training time
    endTime = datetime.datetime.now()
    print('End time:' + endTime.strftime('%Y-%m-%d %H:%M:%S'))
    timedif = endTime - nowTime
    hours, remainder = divmod(timedif.seconds, 3600)
    minutes, seconds = divmod(remainder, 60)
    print('训练历时：' + '%s:%s:%s' % (hours, minutes, seconds))

    # prediction multi gpu
    # predicter.predict_multigpu(train_model,opt,test_dataloader,scaler,predtime)
if __name__ == '__main__':
    main()
