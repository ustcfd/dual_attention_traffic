# coding=utf-8
import torch.nn.functional as F
import torch
import torch.nn as nn
import torch.nn.init as init
from torch.autograd import Variable
from Modules import LSTMCell,Linear
import random
import math
class GraphAttentionModel(nn.Module):
    def __init__(self,adj_bias,feat_in=1,K=8,num_h=128,d_sh=16,feat_out=1,res_steps=4,coef_drop=0.0,in_drop=0.0,ss_k=1000):
        super(GraphAttentionModel,self).__init__()
        self.K=K
        self.feat_out=feat_out
        self.encoder=Encoder(adj_bias,feat_in,K,num_h,d_sh,res_steps,coef_drop,in_drop)
        self.decoder=Decoder(adj_bias,feat_in,K,num_h,d_sh,feat_out,res_steps,coef_drop,in_drop,ss_k)

    def forward(self,inputs,targets,is_training, global_step=None):
        encoder_H,encoder_C,Pred_States=self.encoder(inputs)
        pred_out=self.decoder(targets,encoder_H,encoder_C,Pred_States,is_training,global_step)
        return pred_out


class Encoder(nn.Module):
    def __init__(self,adj_bias,feat_in=1, K=4, num_h=128, d_sh=16,res_steps=4,coef_drop=0.0,in_drop=0.0):
        super(Encoder, self).__init__()
        self.K = K
        self.res_step=res_steps
        self.rnn = LSTMCell(adj_bias,K,feat_in,num_h, d_sh,coef_drop,in_drop)

    def forward(self, inputs):
        batch_size,time_step, node_num, fea_in = inputs.size()
        # 数据格式为[B,T,N,1]
        Hidden_State,C_State,Pred_States = self.rnn.initHidden(self.res_step,batch_size, node_num)
        Pred_States = torch.cat((Pred_States, Hidden_State.unsqueeze(0)), 0)
        for i in range(0, time_step):
            # current_input = inputs[:, :, i, :]
            current_input = torch.squeeze(inputs[:,i:i + 1,:,:], 1)
            pred_state=Pred_States[-self.res_step-1:-1]
            Hidden_State,C_State = self.rnn(current_input, Hidden_State,C_State,pred_state)
            Pred_States=torch.cat((Pred_States,Hidden_State.unsqueeze(0)),0)
        return Hidden_State,C_State,Pred_States

class Decoder(nn.Module):
    def __init__(self,adj_bias,feat_in=1,K=8, num_h=128, d_sh=16,feat_out=1,res_steps=4,coef_drop=0.0,in_drop=0.0,ss_k=1000):
        super(Decoder, self).__init__()
        self.K = K
        self.ss_k=ss_k
        self.res_step = res_steps
        self.rnn = LSTMCell(adj_bias, K, feat_in, num_h, d_sh, coef_drop, in_drop)
        self.full_layer = Linear(num_h, feat_out)
    def forward(self,targets,decoder_hidden,decoder_C,pred_hidden_states,is_training=False,global_step=None):
        k=self.ss_k
        # targets数据格式为[B,T,N,1]
        batch_size, time_step, node_num,fea_in = targets.size()
        # N_State, D_State, _, max_a = self.rnn.initHidden(batch_size, node_num)
        # Hidden_State = decoder_state

        GO_SYMBOL = Variable(torch.zeros(batch_size, 1,node_num, fea_in))
        if torch.cuda.is_available():
            GO_SYMBOL=GO_SYMBOL.cuda()
        dec_input=torch.cat((GO_SYMBOL,targets),1)
        Hidden_State = decoder_hidden
        C_State=decoder_C
        Pred_States=pred_hidden_states
        final_outputs=[]
        if is_training:#training phase-- schedule sampling
            for i in range(0,time_step):
                if i < 1:
                    # 数据格式为[B,N,1]
                    current_input = dec_input[:,i,:,:]
                else:
                    # random() 方法返回随机生成的一个实数，它在[0,1)范围内。
                    if random.random() < self.compute_sampling_threshold(global_step,k):
                        current_input =dec_input[:,i,:,:]
                    else:
                        current_input = predict
                pred_state=Pred_States[-self.res_step-1:-1]
                Hidden_State,C_State = self.rnn(current_input, Hidden_State,C_State,pred_state)
                Pred_States = torch.cat((Pred_States, Hidden_State.unsqueeze(0)), 0)
                # print(Pred_States.size())
                predict=self.full_layer(Hidden_State)
                final_outputs.append(predict)
        else:# test phase
            current_input = torch.squeeze(GO_SYMBOL, 1)
            for i in range(0, time_step):
                pred_state = Pred_States[-self.res_step-1:-1]
                Hidden_State, C_State = self.rnn(current_input, Hidden_State,C_State,pred_state)
                Pred_States = torch.cat((Pred_States, Hidden_State.unsqueeze(0)), 0)
                predict = self.full_layer(Hidden_State)
                current_input = predict
                final_outputs.append(predict)

        final_outputs=torch.cat(final_outputs,2)
        return final_outputs

    def compute_sampling_threshold(self,global_step, k=1000):
        threshold=k / (k + round(math.exp(global_step / k),3))
        return threshold

