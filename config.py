# coding=utf-8
import argparse
parser = argparse.ArgumentParser()
parser.add_argument('--task', type=str, default='METR', choices=['METR', 'PEMS'])
parser.add_argument('--epochs', type=int, default=100)
parser.add_argument('--earlystop_epochs', type=int, default=15)
parser.add_argument('--batch_size', type=int, default=200)
parser.add_argument('--manualSeed', type=int, default=123)
parser.add_argument('--max_step', type=int, default=12)
parser.add_argument('--loss_func', type=str, default='MAPE', choices=['RMSE', 'MAPE', 'MAE'])
parser.add_argument('--learning_rate', type=float, default=1e-3)
parser.add_argument('--K_head', type=int, default=4)

parser.add_argument('--coef_drop', type=float, default=0.2)
parser.add_argument('--in_drop', type=float, default=0.2)
parser.add_argument('--seq_len', type=int, default=12)
parser.add_argument('--pred_len', type=int, default=12, choices=[3, 6, 12])
parser.add_argument('--feat_in', type=int, default=1)
parser.add_argument('--feat_out', type=int, default=1)
parser.add_argument('--num_hidden', type=int, default=64)
parser.add_argument('--d_sh', type=int, default=24)
parser.add_argument('--res_steps', type=int, default=4)
parser.add_argument('--k', type=int, default=1000)

parser.add_argument('--log', type=str, default='gcrn')
parser.add_argument('--summaryDir', type=str, default='./logs/')
parser.add_argument('--checkpointDir', type=str, default='./checkpoint_dir/')
parser.add_argument('--figureDir', type=str, default='./figures/')
parser.add_argument('--save_mode', type=str, choices=['all', 'best'], default='best')

def get_config():
    config = parser.parse_args()
    return config