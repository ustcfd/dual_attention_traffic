# coding=utf-8
import torch.nn.functional as F
import torch
import torch.nn as nn
import torch.nn.init as init
from torch.autograd import Variable
from torch.nn.parameter import Parameter
import math
import os
class Linear(nn.Module):
    ''' Simple Linear layer with xavier init '''
    def __init__(self, d_in, d_out, bias=True,normalize=False):
        super(Linear, self).__init__()
        self._normalize=normalize
        self.linear = nn.Linear(d_in, d_out, bias=bias)
        init.xavier_normal(self.linear.weight)
        self.normalization = LayerNormalization(d_out)
    def forward(self, x):
        res=self.linear(x)
        if self._normalize:
            res=self.normalization(res)
        return res


# self.A_weight=nn.Parameter(torch.Tensor(in_features, out_features), requires_grad=True)
# init.xavier_normal(self.A_weight)

class HeadAttention(nn.Module):
    def __init__(self,adj_bias,num_hidden):
        super(HeadAttention, self).__init__()
        self.bias_mat=adj_bias
        self.num_hidden=num_hidden
        adj_bias =torch.FloatTensor(adj_bias)
        use_gpu = torch.cuda.is_available()
        if use_gpu:
            adj_bias = adj_bias.cuda()
        else:
            adj_bias = adj_bias
        # self.attn_bias=nn.Parameter(torch.Tensor(out_sz))
        # init.constant(self.attn_bias, 0)
        # 利用rnn的hidden计算权重矩阵，将输入数据[B,N,d]，单独两次1*1卷积，[B,N,1]，输出信道为1
        # torch.nn.Conv1d(in_channels, out_channels, kernel_size, stride=1, padding=0, dilation=1, groups=1, bias=True)
        self.conv_1=nn.Conv1d(num_hidden,1,1,1)
        self.conv_2 = nn.Conv1d(num_hidden, 1, 1, 1)
        # self.conv_out = nn.Conv1d(in_sz,out_sz, 1, 1)
        self.register_buffer("adj_bias", adj_bias)
    def forward(self,hidden,residual=True):
        adj_bias = Variable(self.adj_bias, requires_grad=False)
        # seq shape[B,N,d],换为[B,d,N],在pytorch nn.Conv1d中，d视为输入信道数
        # permute或torch.transpose将tensor的维度换位。
        x=hidden.transpose(1,2)
        x1=self.conv_1(x)
        x2=self.conv_2(x).permute(0,1,2)
        # x的shape为[B,N,N]
        x=x1+x2
        adj_attention=F.leaky_relu(x,0.2)+adj_bias
        coefs=F.softmax(adj_attention,2)
        return coefs

class gconv_res(nn.Module):
    def __init__(self, in_sz, out_sz, K,coef_drop=0.0, in_drop=0.0):
        super(gconv_res, self).__init__()
        self.coef_drop = coef_drop
        self.in_drop = in_drop
        self.K=K
        self.conv_out=nn.ModuleList([nn.Conv1d(in_sz, out_sz, 1, 1) for i in range(K)])
    def forward(self, inputs, coefs, residual=True):
        # K,batch_size,node,dim=coefs.size()
        # seq_fts利用1*1卷积核升维，从1维到8维，再与权重矩阵相乘.[B,N,N]*[B,N,out_sz]=[B,N,out_sz]
        res=[]
        for i in range(0,self.K):
            seq_fts = self.conv_out[i](inputs.transpose(1, 2)).transpose(1, 2)
            if self.coef_drop != 0.0:
                coefs = F.dropout(coefs, self.coef_drop)
            if self.in_drop != 0.0:
                seq_fts = F.dropout(seq_fts, self.in_drop)
            ret = torch.bmm(coefs[i], seq_fts)
            if residual:
                if inputs.shape[-1] != ret.shape[-1]:
                    ret = ret + seq_fts  # activation
                else:
                    ret = ret + inputs
            ret = F.elu(ret)
            res.append(ret)
        output=torch.cat(res,2)
        return output

class LSTMCell(nn.Module):
    def __init__(self,adj_bias,K=4,feat_in=1,num_h=64, d_sh=16,coef_drop=0.0,in_drop=0.0,decay=True):
        super(LSTMCell, self).__init__()
        self.hidden_size=num_h
        self.decay=decay
        self.K=K
        self.liner_f = Linear(K * d_sh, num_h,normalize=True)
        self.liner_i = Linear(K * d_sh, num_h,normalize=True)
        self.liner_o = Linear(K * d_sh, num_h,normalize=True)
        self.liner_c = Linear(K * d_sh, num_h, normalize=True)
        self.gconv_attention = nn.ModuleList([HeadAttention(adj_bias, num_h) for i in range(K)])
        self.res_concat = gconv_res(feat_in + 2*num_h, d_sh, K, coef_drop, in_drop)
    def forward(self,inputs, Hidden_State,C_State,Pred_k_State):
        coefs = []
        res_attention_c = self.dot_prod_attention(Hidden_State, Pred_k_State)
        for m in range(0, self.K):
            coefs_single = torch.unsqueeze(self.gconv_attention[m](Hidden_State), 0)
            coefs.append(coefs_single)
        coefs_mat = torch.cat(coefs, 0)
        concat=torch.cat((inputs,res_attention_c,Hidden_State),2)
        gc_concat=self.res_concat(concat,coefs_mat)
        # u,g,z shape[B,N,d]
        f=F.sigmoid(self.liner_f(gc_concat))
        i=F.sigmoid(self.liner_i(gc_concat))
        o=F.sigmoid(self.liner_o(gc_concat))
        c_hat=F.tanh(self.liner_c(gc_concat))
        c=f*C_State+i*c_hat
        h=o*F.tanh(c)
        return h,c

    def initHidden(self,res_k,batch_size,Node):
        use_gpu = torch.cuda.is_available()
        if use_gpu:
            C_State = Variable(torch.zeros(batch_size,Node,self.hidden_size).cuda())
            Hidden_State = Variable(torch.FloatTensor(batch_size, Node, self.hidden_size).cuda())
            init.xavier_normal(Hidden_State)
            Pred_States=Variable(torch.zeros(res_k,batch_size,Node,self.hidden_size).cuda(),requires_grad=False)
            return Hidden_State,C_State,Pred_States
        else:
            C_State = Variable(torch.zeros(batch_size,Node, self.hidden_size))
            Hidden_State = Variable(torch.zeros(batch_size,Node, self.hidden_size))
            init.xavier_normal(Hidden_State)
            Pred_States = Variable(torch.zeros(res_k, batch_size, Node, self.hidden_size),requires_grad=False)
            return Hidden_State,C_State,Pred_States


    def dot_prod_attention(self, h_t, h_pred):
        """
        :param h_t: (batch_size, node_num,hidden_size)
        :param src_encoding: (T,batch_size, src_sent_len, hidden_size)
        """
        # (batch_size, src_sent_len)
        # print(h_pred.contiguous().size())
        b_sz,n_sz,h_sz=h_t.size()
        tensor_k=torch.transpose(h_t,0,1).contiguous().view(n_sz,b_sz*h_sz)
        # (n_sz,T, b_sz*h_sz)
        tensor_q=h_pred.permute(2,0,1,3).contiguous().view(n_sz,-1,b_sz*h_sz)
        att_weight = torch.bmm(tensor_q, tensor_k.unsqueeze(2))
        # print(att_weight.size())
        att_weight = F.softmax(att_weight,1)
        # (n_sz,1,T)
        att_view = (att_weight.size(0), 1, att_weight.size(1))
        # (n_sz,1,T)*(n_sz,T,b_sz*h_sz)=(n_sz,1,b_sz*h_sz)
        ctx_vec = torch.bmm(att_weight.view(*att_view), tensor_q).squeeze(1)
        # (b_sz,n_sz,h_sz)
        ctx_vec=torch.transpose(ctx_vec.view(n_sz,b_sz,h_sz),0,1).contiguous()
        return ctx_vec

class LayerNormalization(nn.Module):
    ''' Layer normalization module '''

    def __init__(self, d_hid, eps=1e-3):
        super(LayerNormalization, self).__init__()

        self.eps = eps
        self.a_2 = nn.Parameter(torch.ones(d_hid), requires_grad=True)
        self.b_2 = nn.Parameter(torch.zeros(d_hid), requires_grad=True)

    def forward(self, z):
        if z.size(1) == 1:
            return z

        mu = torch.mean(z, keepdim=True, dim=-1)
        sigma = torch.std(z, keepdim=True, dim=-1)
        ln_out = (z - mu.expand_as(z)) / (sigma.expand_as(z) + self.eps)
        ln_out = ln_out * self.a_2.expand_as(ln_out) + self.b_2.expand_as(ln_out)

        return ln_out




