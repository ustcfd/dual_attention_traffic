# coding=utf-8
import torch
import utils
from gcrn_model import GraphAttentionModel
import os
import pandas as pd
import predicter
import config

if __name__ == '__main__':
    ''' Main function '''
    opt = config.get_config()
    # 设置预测时间范围长度
    predtime = opt.pred_len * 5
    if predtime < 60:
        predtime = str(predtime) + 'min'
    else:
        n = predtime // 60
        predtime = str(n) + 'hours'

    if opt.save_mode == 'best':
        model_name = opt.save_mode + '_' + predtime + '_' + opt.task + '_' + opt.loss_func + '.chkpt'
        modelfile_path = os.path.join(opt.checkpointDir, model_name)

    checkpoint = torch.load(modelfile_path)
    print(checkpoint['settings'])