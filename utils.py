# coding=utf-8
import torch.utils.data as utils
import torch
import numpy as np
import os
import pickle
import math
# import matplotlib.pyplot as plt
# plt.switch_backend('agg')
class StandardScaler:
    """
    Standard the input
    """
    def __init__(self, mean, std):
        self.mean = mean
        self.std = std
    def transform(self, data):
        return (data - self.mean) / self.std
    def inverse_transform(self, data):
        return (data * self.std) + self.mean


def pklLoad(fname):
    with open(fname, 'rb') as f:
        return pickle.load(f,encoding='latin1')

def pklSave(fname, obj):
    with open(fname, 'wb') as f:
        pickle.dump(obj, f)

# 加载邻接矩阵数据和训练集数据
def adj_to_bias(adj, sizes, nhood=1):
    adj = adj[np.newaxis]
    nb_graphs = adj.shape[0]
    mt = np.empty(adj.shape)
    for g in range(nb_graphs):
        mt[g] = np.eye(adj.shape[1])
        for _ in range(nhood):
            mt[g] = np.matmul(mt[g], (adj[g] + np.eye(adj.shape[1])))
        for i in range(sizes[g]):
            for j in range(sizes[g]):
                if mt[g][i][j] > 0.0:
                    mt[g][i][j] = 1.0
    return -1e9 * (1.0 - mt)

def PrepareDataset(feat_matrix, BATCH_SIZE = 40, seq_len = 12, pred_len = 12, train_propotion = 0.7, valid_propotion = 0.1):
    """ Prepare training and testing datasets and dataloaders.

       Convert speed/volume/occupancy matrix to training and testing dataset.
       The vertical axis of speed_matrix is the time axis and the horizontal axis
       is the spatial axis.

       Args:
           speed_matrix: a Matrix containing spatial-temporal speed data for a network
           seq_len: length of input sequence，输入时间步长
           pred_len: length of predicted sequence，预测时间步长
       Returns:
           Training dataloader
           Testing dataloader
       """
    # 总的样本数
    time_len = feat_matrix.shape[0]
    num_node=feat_matrix.shape[1]
    # 第一个max()q求每一列的最大值，第二个max()从最大值中再求最大特征值
    max_feat = feat_matrix.max().max()
    # 归一化
    # feat_matrix = feat_matrix / max_feat
    # 滑动特征和标签数据窗口
    feat_sequences, feat_labels = [], []
    for i in range(time_len - seq_len - pred_len):
        feat_sequences.append(feat_matrix.iloc[i:i + seq_len].values)
        feat_labels.append(feat_matrix.iloc[i + seq_len:i + seq_len + pred_len].values)
    feat_sequences,feat_labels = np.asarray(feat_sequences), np.asarray(feat_labels)

    sample_size = feat_sequences.shape[0]
    # shuffle and split the dataset to training and testing datasets
    # index = np.arange(sample_size, dtype=int)
    # np.random.shuffle(index)
    # feat_sequences = feat_sequences[index]
    # feat_labels = feat_labels[index]

    train_index = int(np.floor(sample_size * train_propotion))
    valid_index = int(np.floor(sample_size * (train_propotion + valid_propotion)))

    train_data, train_label = feat_sequences[:train_index], feat_labels[:train_index]
    valid_data, valid_label = feat_sequences[train_index:valid_index], feat_labels[train_index:valid_index]
    test_data, test_label = feat_sequences[valid_index:], feat_labels[valid_index:]

    train_data, train_label = torch.Tensor(train_data).view(-1,seq_len,num_node,1), \
                              torch.Tensor(train_label).view(-1,pred_len,num_node,1)
    valid_data, valid_label = torch.Tensor(valid_data).view(-1,seq_len,num_node,1), \
                              torch.Tensor(valid_label).view(-1,pred_len,num_node,1)
    test_data, test_label = torch.Tensor(test_data).view(-1,seq_len,num_node,1), \
                            torch.Tensor(test_label).view(-1,pred_len,num_node,1)
    # 先转换成 torch 能识别的 Dataset
    train_dataset = utils.TensorDataset(train_data, train_label)
    valid_dataset = utils.TensorDataset(valid_data, valid_label)
    test_dataset = utils.TensorDataset(test_data, test_label)
    # 把 dataset 放入 DataLoader
    train_dataloader = utils.DataLoader(train_dataset, batch_size=BATCH_SIZE, shuffle=True, drop_last=True)
    valid_dataloader = utils.DataLoader(valid_dataset, batch_size=BATCH_SIZE, shuffle=True, drop_last=True)
    test_dataloader = utils.DataLoader(test_dataset, batch_size=BATCH_SIZE, shuffle=True, drop_last=True)

    return train_dataloader, valid_dataloader, test_dataloader

def load_adj(adj_fname,nNode):
    adj = pklLoad(adj_fname)
    adj[adj < 0.1] = 0
    adj_bias = adj_to_bias(adj, [nNode])
    return adj_bias



def custom_replace(tensor, on_zero, on_non_zero):
    # we create a copy of the original tensor,
    # because of the way we are replacing them.
    res = tensor.clone()
    res[tensor == 0] = on_zero
    res[tensor != 0] = on_non_zero
    return res

# MAPE:Mean Absolute Perentage Error
def mape_loss(grads,preds):
    # 根据grad生成两个masked tensor，mask1用于识别grad中为0的元素，mask2识别grad中不为0的元素
    mask1 = custom_replace(grads, 1, 0)
    mask2 = custom_replace(grads, 0, 1)
    # 计算grad中不为0的元素的总数目,torch.nonzero返回的是非0元素的索引，取size(0)可以得到个数
    total_num= torch.nonzero(mask2).size(0)
    # 将mask1由torch.FloatTensor转换为torch.ByteTensor
    masked = mask1.type(torch.ByteTensor).cuda()
    # 利用masked_fill_(在mask值为1的位置处用value填充)将grad中为0的值用非零（例如1.0）值替代，防止计算mape时出现分母为0的错误
    grads = grads.masked_fill_(masked, 1.0)

    sub_opt = torch.abs(torch.div(grads - preds, grads))
    # sub_opt * mask2将无效计算的值与0相乘得到0，这样不影响sum操作
    loss = torch.sum(sub_opt * mask2) / total_num
    return loss
def masked_mape_loss(scaler):
    def loss(grads,preds):
        if scaler:
            preds = scaler.inverse_transform(preds)
            grads = scaler.inverse_transform(grads)
        mape = mape_loss(grads=grads,preds=preds)
        return mape
    return loss

# MAE:Mean Absolute Error
def mae_loss(grads,preds):
    # 根据grad生成两个masked tensor，mask2识别grad中不为0的元素
    mask2 = custom_replace(grads, 0, 1)
    # 计算grad中不为0的元素的总数目,torch.nonzero返回的是非0元素的索引，取size(0)可以得到个数
    total_num = torch.nonzero(mask2).size(0)
    sub_opt = torch.abs(grads - preds)
    # sub_opt * mask2将无效计算的值与0相乘得到0，这样不影响sum操作
    loss = torch.sum(sub_opt * mask2) / total_num
    return loss

def masked_mae_loss(scaler):
    def loss(grads,preds):
        if scaler:
            preds = scaler.inverse_transform(preds)
            grads = scaler.inverse_transform(grads)
        mae = mae_loss(grads=grads,preds=preds)
        return mae
    return loss

# RMSE:Root Mean Square Error
def rmse_loss(grads,preds):
    # 根据grad生成两个masked tensor，mask2识别grad中不为0的元素
    mask2 = custom_replace(grads, 0, 1)
    # 计算grad中不为0的元素的总数目,torch.nonzero返回的是非0元素的索引，取size(0)可以得到个数
    total_num = torch.nonzero(mask2).size(0)
    sub_opt = (grads - preds)**2
    # sub_opt * mask2将无效计算的值与0相乘得到0，这样不影响sum操作
    loss = torch.sqrt(torch.sum(sub_opt * mask2)/ total_num)
    return loss

def masked_rmse_loss(scaler):
    def loss(grads,preds):
        if scaler:
            preds = scaler.inverse_transform(preds)
            grads = scaler.inverse_transform(grads)
        rmse = rmse_loss(grads=grads,preds=preds)
        return rmse
    return loss

#matplob
# def annot_min(x,y, ax=None):
#     xmin = x[np.argmin(y)]
#     ymin = y.min()
#     text= "x={:.1f}, y={:.3f}".format(xmin, ymin)
#     if not ax:
#         ax=plt.gca()
#     bbox_props = dict(boxstyle="square,pad=0.3", fc="w", ec="k", lw=0.72)
#     arrowprops=dict(arrowstyle="->",connectionstyle="arc3")
#     kw = dict(xycoords='data',textcoords="axes fraction",
#               arrowprops=arrowprops, bbox=bbox_props, ha="right", va="top")
#     ax.annotate(text, xy=(xmin, ymin), xytext=(1.0,0.2), **kw)
#
# def loss_figure(file_train,file_valid,image_path):
#     # 若设置了unpack=True将返回各列
#     epoch1, train_loss = np.loadtxt(file_train, dtype=[('f0', int), ('f1', float)], delimiter=',', skiprows=1,
#                                     unpack=True)
#     epoch2, valid_loss = np.loadtxt(file_valid, dtype=[('f0', int), ('f1', float)], delimiter=',', skiprows=1,
#                                     unpack=True)
#     plt.plot(epoch1, train_loss)
#     plt.plot(epoch2, valid_loss)
#     plt.gca().set_color_cycle(['red', 'blue'])
#     plt.legend(['train_loss', 'valid_loss'], loc='upper right')
#     annot_min(epoch2, valid_loss)
#     plt.savefig(image_path)



